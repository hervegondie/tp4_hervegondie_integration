
// const page = "formulaire.html"
const page = document.getElementById('forminfolettre')

const boutonForm = document.getElementById('boutonform')
const boutonfermer = document.getElementById('boutonfermer') 

boutonForm.addEventListener('click', afficherPage)
boutonfermer.addEventListener('click', fermerPage)


function afficherPage(){
    page.style.display = "grid"
    boutonForm.style.display = 'none'
}

function fermerPage(){
    page.style.display = "none"
    boutonForm.style.display = 'grid'
}