tp4_hervegondie_integration
tp2 animation: Mon Portfolio animé

Liste des animations et leurs emplacements dans le documents.

1- L'animation par le :Hover sur les éléments du menu est présente sur la barre de navigation de toutes les pages.
2- Des liens ont été ajoutés au début et à la fin des pages pour animer les scroll sur les pages. l'animation du scroll des pages est faite sur la navigation entre le haut et le bas de la page. 
3- l'élément SVG animé se trouve au bas de la page d'accueil. 
4- l'animation de la page contrôlée par un bouton d'appel à l'est faite en bas et à droite de la page d'accueil. Il suffit de cliquer sur le bouton "S'inscrire pour faire apparaître la page.
5- L'animation d'un objet qui fait le tour des pages sous formes de keyframes est faite sur toutes les pages du site.